@extends('layouts.default')
<?php $TITLE = "Welcome to TTL" ?>

@section('scripts')

{{ Html::script('js/material-dashboard.js') }}
{{ Html::script('js/chartist.min.js') }}

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

@stop

@section('styles')

<!--  Material Dashboard CSS    -->
{{ Html::style('css/material-dashboard.css') }}

<!--  CSS for Demo Purpose, don't include it in your project     -->
{{ Html::style('css/demo.css') }}

<!--     Fonts and icons     -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">

<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

@stop

{{-- Define content for dashboard --}}
@section('content')
@include('pages.' . $page )
@stop