<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //  Shows the owner dashboard page
    public function getShowDashboard()
    {
        //  do computations here
        
        return $this->getPage( 'dashboard' );
    }

}
