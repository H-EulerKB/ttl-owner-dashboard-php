<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Tractors</h4>
                <p class="category"></p>
            </div>
            <div class="card-content table-responsive">
                <table class="table" id="tractor-list">
                    <thead class="text-primary">
                        <tr>
                            <th>Operator Name</th>
                            <th>Drivers license</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                            <th>Tractor</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="dv-get-all:profiles:details">
                        @foreach($operators as $item)
                            <tr>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['drivers_license'] }}</td>
                                <td>{{ $item['phone_number'] }}</td>
                                <td>{{ $item['address'] }}</td>
                                <td>{{ $item['tractor'] }}</td>
                                <td>
                                    <button data-delete-id="{{ $item['id'] }}" class="btn btn-primary">Delete</button>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                <div>
                    <button class="btn btn-success" data-toggle="modal" data-target="#operatorModal">Add Operator</button>
                </div>
            </div>
        </div>
    </div>
</div>


