<!--   Core JS Files   -->
{{ Html::script('js/jquery-3.1.0.min.js') }}
{{ Html::script('js/bootstrap.min.js') }}
{{ Html::script('js/material.min.js') }}
{{ Html::script('js/bootstrap-notify.js') }}
{{ Html::script('js/site.js') }}

<script src="{{ asset('js/places-autocomplete.js') }}" ></script>

@if(!empty($page))

<script>
$(function() {
    var selectedTab = '{{ $page }}';
    $("li[data-page='" + selectedTab + "']").addClass('active');
})
</script>

@if($page == 'tractors')

    {{-- Script for deleting tractors --}}
    <script>
    $(function(){
        $("button[data-delete-id]").each(function() {
            var target = $(this);
            target.click(function(){
                if(confirm('Delete selected tractor?')) {
                    var id = target.attr('data-delete-id');
                    window.location.href = "/tractors/delete/" + id;
                }
            })
        })
    })
    </script>

    {{-- Laravel Javascript Validation --}}
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>

    {!! jsvalidator::formRequest('App\Http\Requests\TractorRequest','#tractor-form') !!}

   

@elseif($page == 'operator')

 {{-- Script for deleting operators --}}
    <script>
    $(function(){
        $("button[data-delete-id]").each(function() {
            var target = $(this);
            target.click(function(){
                if(confirm('Delete operator?')) {
                    var id = target.attr('data-delete-id');
                    window.location.href = "/operators/delete/" + id;
                }
            })
        })
    })
    </script>

    {{-- Laravel Javascript Validation --}}
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! jsvalidator::formRequest('App\Http\Requests\OperatorRequest','#operator-form') !!}

@endif


    <script>
    @if(Session::has('status'))
        $(function(){
            $.notify("{{ Session::get('status') }}");
        })
    @endif

    @if(!empty($errors->all()))
        $(function()
        {
             $.notify({ title:  "Failed!",
                        message: "{{ $errors->first() }}"
                });
        })
        
    @endif
    </script>

@endif
