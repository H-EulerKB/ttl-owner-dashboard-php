<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    private $_connection;

    public function connection($keep = false)
    {
        if(empty($_connection))
            $_connection =  \App\ConnectionHelper::Create();

        return $_connection;
    }

    
    

    //  Gets the view with the specified page
    protected function getPage($page)
    {
        return view('shared.sectioned',[ 'page' => $page ]);
    }
}

