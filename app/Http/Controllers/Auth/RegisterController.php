<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Devless\SDK\SDK;
use App\Http\Requests\RegisterRequest;
use App\TTLUSSDHelpers;
use App\Utils;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /*  Register user
    */
    public function register(RegisterRequest $request)
    {
        //
        $firstname  = '';
        $lastname = '';
        Utils::SplitName($request->name, $firstname , $lastname );

        $response = $this->connection()->call('devless','signUp', 
        [ 
             'username' => $request->phone , 
             'first_name' => $firstname,
             'last_name' => $lastname,
             'password' => $request->password , 
             'phone_number' => $request->phone,
             'email' => $request->email,
             'phone' => $request->phone 
        ]);
        
        //
        $token = $response['payload']['result']['token'];

        //  check registration code
        if(empty($token))
        {
            //  failed
            return redirect()->back()->withErrors([ $response['payload']['result'] ]);
        }
        else
        {
            //  get profile
            $profile = $response['payload']['result']['profile'];

            //
            $this->connection()->setUserToken($token);

            //  store token for session
            $request->session()->put('token', $token );
            $request->session()->put('user_profile',$profile);

            //  update account type
            $owners = TTLUSSDHelpers::GetOwners( $this->connection() , $profile['phone_number'] );
            if(empty($owners))
            {
                //  no user found
                TTLUSSDHelpers::CreateOwner($this->connection(), 
                [
                    'no_of_tractors' => '0',
                    'location' => 'not set',
                    'type' => $request->admin_type,
                    'phone' => $profile['phone_number'],
                    'name' => $request->name,
                ]);
            }
            else
            {
                //  update info
            }

            //
            return redirect('/dashboard');
        }
        
    }
    
}
