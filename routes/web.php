<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route dashboard */
Route::get('/','DashboardController@getShowDashboard');
Route::get('/dashboard','DashboardController@getShowDashboard');
Route::get('/dashboard/index','DashboardController@getShowDashboard');

/* Route tractors */
Route::get('/tractors','TractorsController@getShowTractors');
Route::get('/tractors/index','TractorsController@getShowTractors');
Route::post('/tractors/create','TractorsController@createTractor');
Route::get('/tractors/delete/{id}','TractorsController@deleteTractor');

/* Route operators */
Route::get('/operators','OperatorsController@getShowOperators');
Route::get('/operators/index','OperatorsController@getShowOperators');
Route::post('/operators/create','OperatorsController@createOperator');
Route::get('/operators/delete/{id}','OperatorsController@deleteOperator');

/* Route acreage */
Route::get('/acreage','AcreageController@getShowAcreage');
Route::get('/acreage/index','AcreageController@getShowAcreage');

/* Route profile */
Route::get('/user','UserController@getShowProfile');
Route::get('/user/profile','UserController@getShowProfile');

/* Authentication routes */
Auth::routes();