<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function messages()
    {
        return
        [
            'phone.required' => 'Phone number is required!',
            'phone.min' => 'Insufficient phone number length',
            'phone.max' => 'Invalid phone number',
            'admin_type.required' => 'Account type is required',
            'name.required' => 'Please supply your full name',
            'name.max' => 'Max name characters exceeded!',
            'email.required' => 'Please supply your email',
            'password.required' => 'Please enter your password',
            'password_confirm.same' => 'Confirm password does not match password',
        ];
    }


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            'phone' => 'required|min:10|max:256|',
            'admin_type' => 'required|string',
            'name' => 'required|string|max:255',
            'email' => 'string|email|max:255|unique:users',
            'password' => 'required|string|min:3',
            'password_confirm' => 'required|string|min:3|same:password',
        ];
    }
}
