<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TractorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Session::has('token');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            'name' => 'required',
            'chasis_no' => 'required',
            'model' => 'required',
            'engine' => 'required',
            'model_year' => 'required|numeric',
            'location' => 'required'
        ];
    }
}
