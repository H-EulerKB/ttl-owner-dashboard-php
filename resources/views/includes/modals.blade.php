@if(!empty($page))

    @if($page == 'tractors')
    
    {{-- Define tractor modals here --}}
    <div class="modal fade" id="tractorModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="" >Add Tractors</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open([ 'url' => '/tractors/create' , 'class' => 'form-horizontal' , 'id' => 'tractor-form'  ]) }}
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="eg. Valtra" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Chasis No." class="col-sm-2 control-label">Chassis No.</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="chasis_no" id="chasis_no" placeholder="eg. AAAT0002KFC002387" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Model" class="col-sm-2 control-label">Model</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="model" id="model" placeholder="eg. A740" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Model" class="col-sm-2 control-label">Engine</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="engine" id="engine" placeholder="eg. B532580A" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Model Year" class="col-sm-2 control-label">Model Year</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="model_year" id="model_year" placeholder="eg. 2005" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="location" class="col-sm-2 control-label">Location</label>
                            <div class="col-sm-10">
                                <input type="text" class="location-autocomplete form-control" name="location" id="location" placeholder="Enter location" required></input>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Save" id="save_tractor" />
                        </div>

                        {{ Form::close() }}
                </div>
            </div>

            <div class="modal-footer">
            </div>
        </div>

    </div>

    @elseif($page == 'operator')

    {{-- Define operator modals here --}}
     <div class="modal fade" id="operatorModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="" >Add Tractor Operator</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open([ 'url' => '/operators/create' , 'class' => 'form-horizontal' , 'id' => 'operator-form'  ]) }}
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="eg. Kwame Darko" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Chasis No." class="col-sm-2 control-label">Phone number</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="eg. 0201231231" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Model" class="col-sm-2 control-label">Drivers license</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="license" id="license" placeholder="eg. Drivers license" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Model" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="address" id="address" placeholder="eg. Accra, Osu" required />
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="Model" class="col-sm-2 control-label">Tractor</label>
                            <div class="col-sm-10">
                                <select name="tractor" id="tractor">
                                    @foreach($tractors as $tractor)
                                        <option value="{{ $tractor['id'] }}">{{ $tractor['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Add Operator" id="save_operator" />
                        </div>

                        {{ Form::close() }}
                </div>
            </div>

            <div class="modal-footer">
            </div>
        </div>

    </div>

    @endif

@endif
