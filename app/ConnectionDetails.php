<?php
namespace App;

class ConnectionDetails
{
    protected $_address;
    protected $_secret;

    public function getAddress()
    {
        return $this->_address;
    }

    public function getSecret()
    {
        return $this->_secret;
    }

    public function __construct( $address  , $secret )
    {
        $this->_address = $address;
        $this->_secret = $secret;
    }
}

