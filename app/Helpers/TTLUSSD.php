<?php

namespace App;

class TTLUSSDHelpers
{
    //  The name of the ussd service
    const SERVICE_NAME = "TTLUSSD";

    //  Owners table
    const OWNERS_TABLE = "owners";

    //  An unsassigned field value
    const UNASSIGNED_FIELD = "unassigned";

    //  A completed field value
    const COMPLETED_FIELD = "completed";

    //  The table name for tractors
    const TRACTORS_TABLE = "tractors";

    //  The table name for tractor requests
    const REQUESTS_TABLE = "tractor_requests";

    //  The table name for operators
    const OPERATORS_TABLE = "operators";


    const SUCCESS_ADD_CODE = 609;

    const SUCCESS_DELETE_CODE = 636;

    private static function SucceededCreate($response)
    {
        var_dump($response);
        if($response['status_code'] == self::SUCCESS_ADD_CODE)
            return true;

        return false;
    }

    private static function SucceededDelete($response)
    {
        if($response['status_code'] == self::SUCCESS_DELETE_CODE)
            return true;

        return false;
    }

    public static function GetOperator($sdk,$id)
    {
        return self::GetResults( $sdk->where('id',$id)->getData( self::SERVICE_NAME , self::OPERATORS_TABLE ) );
    }

    public static function GetOperators($sdk , $id)
    {
        return self::GetResults( $sdk->where( 'TTLUSSD_owners_id' , $id )->getData( self::SERVICE_NAME , self::OPERATORS_TABLE ) );
    }

    public static function CreateOwner($sdk , $properties)
    {
        return self::SucceededCreate( $sdk->addData(self::SERVICE_NAME , self::OWNERS_TABLE , $properties) );
    }

    public static function CreateOperator($sdk , $properties )
    {
        $response = $sdk->addData(self::SERVICE_NAME , self::OPERATORS_TABLE , $properties);
        return self::SucceededCreate( $response );
    }

    public static function DeleteOperator($sdk , $id)
    {
         return self::SucceededDelete( $sdk->where('id',$id)->deleteData( self::SERVICE_NAME , self::OPERATORS_TABLE ) );
    }

    public static function CreateTractor( $sdk , $properties)
    {
        return self::SucceededCreate( $sdk->addData(self::SERVICE_NAME , self::TRACTORS_TABLE , $properties) );
    }

    public static function DeleteTractor( $sdk , $id )
    {
        return self::SucceededDelete( $sdk->where('id',$id)->deleteData( self::SERVICE_NAME , self::TRACTORS_TABLE ) );
    }

    public static function GetUserProfile($sdk)
    {
        $response = $sdk->call('devless','profile' , [] );
        return self::GetResult( $response, 'profile' );
    }

    public static function GetTractorRequests($sdk  , $phone )
    {
        //  get owners
        $owners = self::GetOwners($sdk, $phone);
        
        $sdk = \App\ConnectionHelper::Create();
        $tractors = self::GetTractorsForOwners( $sdk , $owners );
        if(empty($tractors))
        {
            //  there are no tractors available
            return [];
        }

        //
        $sdk = \App\ConnectionHelper::Create();
        foreach($tractors as $tractor)
            $sdk->orWhere('status',$tractor['name']);
        
        //
        return self::GetResults($sdk->getData(self::SERVICE_NAME , self::REQUESTS_TABLE ));
    }

    public static function GetTractor($sdk , $id)
    {

        $results = self::GetResults( $sdk->where('id',$id)->getData(self::SERVICE_NAME  , self::TRACTORS_TABLE) );
        if(empty($results))
            return null;
        return $results[0];
    }

    public static function  GetTractorNames($sdk)
    {
        $results = self::GetResults($sdk->getData(self::SERVICE_NAME  , self::TRACTORS_TABLE));
        if(empty($results))
        {
            //  no tractors
            return [];
        }

        $names  = [];
        foreach( $results as $tractor )
            array_push($names,$tractor['name']);

        return $names;
    }

    //  Fetches all assigned tractors tractors 
    public static function GetInActiveTractors($sdk)
    {
         $tractors = $sdk->where('status', self::UNASSIGNED_FIELD )->getData(self::SERVICE_NAME , self::TRACTORS_TABLE ) ;
         return self::GetResults( $tractors );
    }

    public static function GetTractorsForOwners($sdk , $owners = [])
    {
        $tractors = [];
        foreach($owners as $owner)
        {
            $results = self::GetResults( $sdk->where('TTLUSSD_owners_id' , $owner['id'] )->getData( self::SERVICE_NAME  , self::TRACTORS_TABLE  ) );
            if(!empty($results))
            {
                foreach($results as $tractor)
                {
                    array_push( $tractors , $tractor );
                }
            }
        }

        return $tractors;
    }


    public static function GetOwners( $sdk , $phone )
    {
         return self::GetResults( $sdk->where('phone',$phone)->getData(self::SERVICE_NAME, self::OWNERS_TABLE ) );
    }

    public static function GetResult($response ,string $key = NULL)
    {
        if($key == NULL)
            return $response['payload']['result'];
        else
            return $response['payload']['result'][$key];
    }

    public static function GetResults($response)
    {
        return $response['payload']['results'];
    }
}
