@extends('layouts.user_input')
<?php $TITLE = 'TTL Admin - Login' ?>

@section('scripts')

    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! jsvalidator::formRequest('App\Http\Requests\LoginRequest','#login-form') !!}

@endsection

@section('content')
<div class="input-page">
    {{ Form::open([ 'url' => 'login' , 'method' => 'POST' , 'class' => 'form' , 'id' => 'login-form' ]) }}
            <div class="login-form">
                <h3 class="input-page-header text-left">Login</h3>
                <div class="input-frame">
                    {{ Form::text('phone', old('phone') , [ 'placeholder' => 'phone number' ]) }}
                </div>

                <div class="input-frame">
                    {{ Form::password('password', [ 'placeholder' => 'password' ] ) }}
                </div>

                @if(!empty($errors->all()))
                    <ul class="validation-msg">
                        @foreach($errors->all() as $item)
                            <li>{{$item}}</li>
                        @endforeach
                    </ul>
                @endif

                <button type="submit">Login</button>
                <p class="message">Not registered? <a href="{{ url('register') }}">Create new account</a></p>
            </div>
    {{ Form::close() }}
</div>
@stop
