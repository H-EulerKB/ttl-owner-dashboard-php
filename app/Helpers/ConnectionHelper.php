<?php 

namespace App;

class ConnectionHelper
{
    public static function Create()
    {
        $details = new \App\ConnectionDetails(config('app.devless_address'), config('app.devless_secret'));
        $connection = new \Devless\SDK\SDK($details);
        if(\Session::has('token'))
        {
            $connection->setUserToken(\Session::get('token'));
        }

        return $connection;
    }

}
