<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Tractors</h4>
                <p class="category"></p>
            </div>
            <div class="card-content table-responsive">
                <table class="table" id="tractor-list">
                    <thead class="text-primary">
                        <tr>
                            <th>Id</th>
                            <th>Brand Name</th>
                            <th>Chasis No.</th>
                            <th>Model</th>
                            <th>Location</th>
                            <th>Price Per Acre</th>
                            <th>Model Year</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="dv-get-all:profiles:details">
                        @foreach($tractors as $item)
                            <tr>
                                <td>{{ $item['id'] }}</td>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['chasis'] }}</td>
                                <td>{{ $item['model'] }}</td>
                                <td></td>
                                <td></td>
                                <td>{{ $item['year'] }}</td>
                                <td>
                                <button data-delete-id="{{ $item['id'] }}" class="btn btn-primary">Delete</button>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                <div>
                    <button class="btn btn-success" data-toggle="modal" data-target="#tractorModal">Add Tractors</button>
                </div>
            </div>
        </div>
    </div>
</div>


