<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>{{ $TITLE }}</title>
    @include('includes.head')
    @yield('styles')
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="green" data-image="{{ asset('image/sidebar-1.jpg') }}">
        	<div class="logo">
				<a href="{{ config('app.ttl_site') }}" class="simple-text"><img src="{{ asset('image/logo.png') }}" alt="" height="66" width="124"></a>
			</div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="{{ URL::to('/user') }}">
                                <img class="img" src="{{ asset('image/faces/marc.jpg') }}" alt="" height="100" />
                            </a>
                        </div>
                    </div>
                    <li data-page="dashboard">
                        <a href="{{ URL::to('dashboard') }}">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li data-page="profile">
                        <a href="{{ URL::to('user') }}">
                            <i class="material-icons">person</i>
                            <p>Profile</p>
                        </a>
                    </li>
                    <li data-page="acreage">
                        <a href="{{ URL::to('acreage') }}">
                            <i class="material-icons">room_service</i>
                            <p>Acreage</p>
                        </a>
                    </li>
                    <li data-page="operators">
                        <a href="{{ URL::to('operators') }}">
                            <i class="material-icons">build</i>
                            <p>Tractor Oprators</p>
                        </a>
                    </li>
                    <li data-page="tractors">
                        <a href="{{ URL::to('tractors') }}">
                            <i class="material-icons">event_seat</i>
                            <p>Tractor</p>
                        </a>
                    </li>
                    <li data-page="maps">
                        <a href="{{ URL::to('map') }}">
                            <i class="material-icons">location_on</i>
                            <p>Maps</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        @yield('title')
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            
                            {{-- Define the logout form --}}
                            <form style="display:hidden" action="/logout" method="POST" id="logout-form">
                                {{ csrf_field() }}
                            </form>

                            <li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="material-icons">person</i>
									<p class="hidden-lg hidden-md">Account</p>
								</a>
								<ul class="dropdown-menu">
									<li><a href="{{ URL::to('/user') }}">View Profile</a></li>
                                    <li class="seperator"></li>
									<li><a href="javascript:document.forms['logout-form'].submit();">Logout</a></li>
								</ul>
							</li>
                        </ul>
                        {{ Form::open( [ 'url' => '' , 'role' => 'search' , 'class' => 'navbar-form navbar-right' ] ) }}
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i><div class="ripple-container"></div>
                            </button>
                        {{ Form::close() }}
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
                <p class="copyright pull-right">
                   @include('includes.footer')
                </p>
            </div>
        </div>
    </div>

    {{-- Load required scripts here --}}
    @include('includes.scripts')

    {{-- Setup modals --}}
    @include('includes.modals')

    @yield('scripts')
    
</body>

</html>
