<?php

namespace App\Http\Middleware;
use Illuminate\Auth\AuthenticationException;
use Closure;
use Session;

class DevlessAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->has('token'))
            throw new AuthenticationException('Unauthenticated');
        
        return $next($request);
    }
}
