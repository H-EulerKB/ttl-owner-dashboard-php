<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\OperatorRequest;
use \App\TTLUSSDHelpers;

class OperatorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('valid_user');
    }

    //  Shows the tractor operators page
    public function getShowOperators(Request $request)
    {
        //
        $sdk =  \App\ConnectionHelper::Create();
        $operators = TTLUSSDHelpers::GetOperators($sdk , $request->session()->get('user_profile')['id'] );

        //
        $sdk = \App\ConnectionHelper::Create();
        $owners = TTLUSSDHelpers::GetOwners($sdk , $request->session()->get('user_profile')['phone_number'] );

        //
        $sdk  = \App\ConnectionHelper::Create();
        $tractors = TTLUSSDHelpers::GetTractorsForOwners($sdk, $owners);

        $ts = [];
        foreach($operators as $operator)
        {
            $operator['tractor'] = TTLUSSDHelpers::GetTractor($this->connection(), $operator['TTLUSSD_tractors_id'] )['name'] ;
            array_push($ts, $operator );
        }

        return $this->getPage( 'operator' )->with('operators',$ts)->with('tractors',$tractors);
    }

    public function createOperator(OperatorRequest $request)
    {
        if( TTLUSSDHelpers::CreateOperator( $this->connection() , 
        [
            'TTLUSSD_owners_id' => $request->session()->get('user_profile')['id'],
            'name' => $request->name,
            'phone_number' => $request->phone,
            'drivers_license' => $request->license,
            'address' => $request->address,
            'TTLUSSD_tractors_id' => $request->tractor
        ]))
            $request->session()->flash('status','Operator created successfully!');
        else
            $request->session()->flash('status','Failed creating operator!');

        return redirect()->back();
    }

    public function deleteOperator(Request $request, $id)
    {
        if(TTLUSSDHelpers::DeleteOperator($this->connection() , $id ))
            $request->session()->flash('status','Deleted operator successfully!');
        else
            $request->session()->flash('status','Failed deleting operator!');

        return redirect()->back();
        
    }
}
