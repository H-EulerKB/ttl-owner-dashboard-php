<?php

namespace App;

class Utils
{
    public static function SplitName($name , &$fname , &$lname)
    {
        $space = strpos($name, " ");
        if($space > 0)
        {
            $fname = substr($name,0,$space);
            $lname = substr($name,$space);
        }
        else
        {
            $fname = $name;
        }

    }
}

?>