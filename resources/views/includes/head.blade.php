<meta charset="utf-8" />

<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-icon.png') }}" />

<link rel="icon" type="image/png" href="{{ asset('image/favicon.png') }}" />

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

<meta name="viewport" content="width=device-width" />

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

 <!-- Bootstrap core CSS -->
{{ Html::style('css/bootstrap.min.css') }}

{{-- Define page title here --}}
@section('title')

@if(!empty($page))

    @if($page == 'dashboard')
        <h2>Welcome, {{ Session::get('user_profile')['first_name'] }}</h2>
    @elseif($page == 'tractors')
        <h3>Tractor List<h3>
    @elseif($page == 'operator')
        <h3>Tractor Operators</h3>
    @elseif($page == 'acreage')
        <h3>Acreage</h3>
    @elseif($page = 'profile')
        <h3>Profile</h3>
    @endif
    

@endif

@endsection

@section('styles')

@if(!empty($page))

    @if($page == 'dashboard')

    <!--  Charts Plugin -->
    {{ Html::script('js/chartist.min.js') }}

	<script type="text/javascript">
    	$(document).ready(function(){

			// Javascript method's body can be found in assets/js/demos.js
        	demo.initDashboardPageCharts();

    	});
	</script>

    @endif
    
@endif

@endsection