<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TTLUSSDHelpers;
class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    //  Shows the user profile
    public function getShowProfile(Request $request)
    {
        return $this->getPage('profile')->with('profile',$request->session()->get('user_profile') );
    }
}
