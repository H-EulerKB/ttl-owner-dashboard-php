<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TractorRequest;
use App\TTLUSSDHelpers;

class TractorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        //  This will ensure we have at least one owner record for the user
        $this->middleware('valid_user');

    }

    //  Shows the tractors page
    public function getShowTractors(Request $request)
    {
        $phone = $request->session()->get('user_profile')['phone_number'];
        $owners = TTLUSSDHelpers::GetOwners($this->connection() , $phone );
        $tractors = [];
        if(!empty( $owners) )
           $tractors = TTLUSSDHelpers::GetTractorsForOwners( $this->connection() , $owners );
            
        return $this->getPage( 'tractors' )->with('tractors',$tractors);
    }
    
    //  Create tractor
    public function createTractor(TractorRequest $request)
    {
        //  get phone number
        $phone = $request->session()->get('user_profile')['phone_number'];

        //  find owners
        $owners = TTLUSSDHelpers::GetOwners($this->connection() , $phone );

        //  get first owner
        $firstOwner = $owners[0];

        //  create tractor
        if(TTLUSSDHelpers::CreateTractor( $this->connection() , 
        [
            'chasis' => $request->chasis_no ,
            'engine' => $request->engine,
            'model' => $request->model,
            'name' => $request->name,
            'status' => 'unassigned',
            'TTLUSSD_owners_id' => $firstOwner['id'],
            'year' => $request->model_year
        ]))
        {
            $request->session()->flash('status','Tractor was created successfully!');
            return redirect('/tractors');
        }
        else
        {
            $request->session()->flash('status','Failed creating tractor!');
            return redirect()->back();
        }
    }

    //  Delete tractor
    public function deleteTractor(Request $request , $id)
    {
        if(TTLUSSDHelpers::DeleteTractor($this->connection(),$id))
        {
            $request->session()->flash('status','Tractor deleted successfully!');
            return redirect('/tractors');
        }
        else
        {
            $request->session()->flash('status','Operation failed');
            return redirect()->back();
        }
    }
}
