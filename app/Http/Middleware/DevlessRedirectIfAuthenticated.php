<?php

namespace App\Http\Middleware;
use Devless\SDK\SDK;
use Closure;

class DevlessRedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('token'))
            return redirect('/dashboard');
        
        return $next($request);
    }
}
