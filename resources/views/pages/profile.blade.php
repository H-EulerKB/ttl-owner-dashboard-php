<div class="row">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h3 class="title"><i class="material-icons">person</i> Account Details</h3>
        </div>
        <div class="card-content">
        <div>
             <dl>
                <dt>Phone Number</dt>
                <dd>{{ $profile['phone_number'] }}  </dd>
                <dt>Name</dt>
                <dd>{{ $profile['first_name'] . $profile['last_name'] }} <a href="{{ URL::to('/user/changename') }}">Change</a></dd>

                {{-- Check whether email is valid --}}
                @if(!empty($profile['email']))
                <dt>Email</dt>
                <dd>{{$profile['email'] }}</dd>
                @endif

            </dl> 

        </div>

    </div>



</div>