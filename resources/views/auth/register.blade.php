@extends('layouts.user_input')
<?php $TITLE = 'TTL Admin - Register' ?>

@section('scripts')

    <!-- Registration Form Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>

    {!! jsvalidator::formRequest('App\Http\Requests\RegisterRequest','#register-form') !!}

@endsection

@section('content')
<div class="input-page">
        {{ Form::open(  [ 'url' => 'register' , 'method' => 'POST' , 'class' => 'form' , 'id' => 'register-form' ] ) }}
            <div class="login-form">
                <h4 class="text-left input-page-header">Register Account</h4>
                
                <ul id="validation-errors" style="color:red;text-align:left"></ul>

                <div class="input-frame">
                    {{ Form::text('name', old('name') , [ 'placeholder' => 'full name' ] ) }}
                </div>
                <div class="input-frame">
                    {{ Form::text('phone', old('phone') , [ 'placeholder' => 'phone number' ] ) }}
                </div>
                <div class="input-frame">
                    {{ Form::password('password' , [ 'placeholder' => 'password' ] ) }}
                </div>
                
                <div class="input-frame">
                    {{ Form::password('password_confirm' , [ 'placeholder' => 'confirm password' ] ) }}
                </div>

                <p class="text-left"> Select account type</p>
                {{ Form::select('admin_type' , [ 'individual' => 'Individual' , 'company' => 'Company' ]  , old('admin_type') ) }}

                @if(!empty($errors->all()))
                    <ul class="validation-msg">
                    @foreach($errors->all() as $item)
                        <li>{{ $item }}</li>
                    @endforeach
                    </ul>
                @endif
                


                <button type="submit">Register</button>
                <p class="message">Already registered? <a href="{{ url('login') }}">Sign In</a></p>

            </div>
        {{ Form::close() }}

</div>
@endsection
