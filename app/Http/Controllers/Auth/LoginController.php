<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use App\Http\Requests\LoginRequest;
use \Devless\SDK\SDK;

class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    public function login(LoginRequest $request)
    {
        $response = $this->connection()->call('devless','login', ['username'=> $request->phone ,'password'=> $request->password ]);
        if($response['payload']['result'] == false)
        {
            return redirect()->back()->withErrors(['Invalid login attempt. Try again!']);
        }
        else
        {
            //  get token
            $token = $response['payload']['result']['token'];

            //
            $this->connection()->setUserToken($token);

            //  store token for session
            $request->session()->put('token', $token );
            $request->session()->put('user_profile', $response['payload']['result']['profile'] );

            //
            $request->session()->flash('status', "You've been logged in successfully");

            return redirect()->intended('/dashboard');
        } 
    }
 
}
