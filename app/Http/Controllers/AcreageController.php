<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TTLUSSDHelpers;

class AcreageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //  Shows the acreage page
    public function getShowAcreage(Request $request)
    {
        $requests = TTLUSSDHelpers::GetTractorRequests( $this->connection() , $request->session()->get('user_profile')['phone_number']  ); 
        return $this->getPage( 'acreage' )->with( 'requests' ,  $requests );
    }

}
