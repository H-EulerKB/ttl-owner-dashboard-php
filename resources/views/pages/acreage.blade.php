<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Acreage</h4>
                <p class="category"></p>
            </div>
            <div class="card-content table-responsive">
                <table class="table" id="request-table">
                    <thead class="text-primary">
                        <tr>
                            <th>Service Type</th>
                            <th>Acres</th>
                            <th>Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--<tr>
                            <td class="var-firstname">Eve</td>
                            <td class="var-lastname">Jackson</td>
                            <td class="var-age">94</td>
                        </tr>-->
                        @if(empty($requests))
                        @else
                        
                        @foreach($requests as $req)
                        <tr>
                            <td>{{ $req['type'] }}</td>
                            <td>{{ $req['acres'] }}</td>
                            <td>{{ $req['cost'] }}</td>
                        </tr>
                        @endforeach

                        @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>