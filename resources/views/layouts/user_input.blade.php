<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>

    <title>{{ $TITLE }}</title>
    
    @include('includes.head')
    
    @include('includes.user_input_styles')
    
    @yield('styles')

</head>

<body>

    @yield('content')

    {{-- Load required scripts here --}}
    @include('includes.scripts')

    @yield('scripts')

</body>

</html>
