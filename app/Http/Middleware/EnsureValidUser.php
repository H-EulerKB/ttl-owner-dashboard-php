<?php

namespace App\Http\Middleware;

use Closure;

class EnsureValidUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // ensure the current user has a valid record

        return $next($request);
    }
}
