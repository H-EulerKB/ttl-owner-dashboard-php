<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function messages()
    {
        return
        [
            'phone.required' => 'Please enter phone number!',
            'phone.min' => 'Insufficient phone number length',
            'password.required' => 'Please enter your password!'
        ];
    }

    public function authorize()
    {
        return true;
    }
   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            'phone' => 'required|min:10' ,
            'password' => 'required|min:3' 
        ];
    }
}
