<?php

namespace App\Http\Middleware;

use Closure;

class RestoreDevlessToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //  This is for automatic login for users
        if($auth = $request->cookie('token') && !$request->session()->has('token'))
            $request->session()->put('token', $auth);

        return $next($request);
    }
}
